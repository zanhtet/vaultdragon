const sessionModel = require('../models/session.model');

exports.get = (key, timestamp, callback) => {
    let query = {
        key: key
    }

    if (timestamp){
        query.timestamp = timestamp;
    }

    sessionModel.find(query, (err, savedSession) => {
        if (err) {
            return next(err)
        }
        
        if (callback){
            callback(savedSession);
        }
    });
}

exports.save = (sessions, callback) => {
    let sessionsArr = [];

    sessions.forEach(sessionEle => {
        let session = new sessionModel(
            {
                key: sessionEle.key,
                value: sessionEle.value,
                timestamp: sessionEle.timestamp
            }
        );

        sessionsArr.push(session);
    });

    sessionModel.insertMany(sessionsArr, err => {
        if (err) {
            return next(err)
        }

        if (callback){
            callback(sessionsArr);
        }
    });
}