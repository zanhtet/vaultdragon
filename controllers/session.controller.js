const sessionServices = require('../services/sessionServices');

exports.getValue = (req, res) => {
    sessionServices.get(req.params.key, req.query.timestamp, (sessions) => {
        let returnObjArr = [];
        sessions.forEach(session => {
            returnObjArr.push({
                Value: session.value
            });
        });
        res.send(returnObjArr);
    });
};

exports.setValue = (req, res) => {
    let timestamp = new Date().getTime();
    let sessions = [];
    let err = null;

    Object.keys(req.body).forEach(key => {
        let value = req.body[key];

        if (key.length > 10 || value.length > 10){
            err = "key or value cannot be long than 1000 words";
        } else {
            sessions.push({
                key: key,
                value: value,
                timestamp: timestamp
            });
        }
    });

    if (sessions.length > 0) {
        sessionServices.save(sessions);
    }

    if (err){
        res.send(err);
    } else {
        res.send(sessions);
    }
    
};

exports.test = (req, res) => {
    res.send('hello welcome');
}