const mongoose = require('mongoose');
const schema = mongoose.Schema;

let SesionSchema = new schema({
    key: {type: String, required: true, max: 1000},
    value: {type: String, required: true, max: 1000},
    timestamp: {type: Number, required: true}
});

module.exports = mongoose.model('Session', SesionSchema);