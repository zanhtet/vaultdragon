const express = require('express');
const router = express.Router();

const sessionController = require('../controllers/session.controller');

router.get('/:key', sessionController.getValue);
router.post('/', sessionController.setValue);
router.get('/test', sessionController.test);

module.exports = router;