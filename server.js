const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const port=process.env.port || 8080

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const session = require('./routes/session.route');

app.use('/object', session);

let dev_db_url = 'mongodb+srv://sa:system@session-ltit4.mongodb.net/test?retryWrites=true';
let mongoDB = dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});